<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>My Page Title</title>
        <meta name="description" content="Your page description goes here />
        <link href="cssfile.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <a id="container">

            <ul classe="menu" style="list-style-type: square">
                <li>Option 1</li>
                    <ul><lo>XXXX</lo><lo>YYYY</lo></ul>
                <li>Option 2</li>
                <li>Option 3</li>
                <li>Option 4</li>
                <li>Option 5</li>
            </ul>

            <P>In the summer time, when it's a hot day, we like to go to the beach and look out at the sea. Some of us
                go surfing, some of us sit on the beach and soak up the sun, some of us read a book, some of us play
                beach ball or badminton if it isn't too windy. But we all eat a nice picnic together and have a good
                laugh!
                <a href="http://www.staggeringbeauty.com/" target="_self"></a><img src="awesome.png" title="awesome" alt="awesome">
            </P>
            <p>
                <a src="/my-beach-picnic-ideas/" target="blank">Here are some picnic ideas for your day at the
                    beach!</a><br/>
            </p>

            <header>Don't forget:</header>
            <table cellpadding="10" cellmargin="5" border="2">
                <thead>
                    <th>Activity</th><th>Price</th>
                </thead>
                <tbody>
                    <tr><td>Surfiing</td><td>$20</td></tr>
                    <tr><td>Swimming</td><td>free</td></tr>
                    <tr><td>Bathing</td><td>free</td></tr>
                </tbody>
                <tfoot>
                    <tr><td>Total</td><td>$20</td></tr>
                </tfoot>
            </table>

        </div>
    </body>
</html>