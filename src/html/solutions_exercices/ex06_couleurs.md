# Réponses à l'exercice 6 slide 10

1. convertir les couleurs entre RGB et HEX:

    rgb(0, 0, 255)   =>  #0000FFF
    #3F03B1          =>  rgb(63, 3, 177) 
        
    3*16+15*1=48+15 = 63
    0*16+3*1=0+3
    11*176+1*1=177
    
    #BB44AC   =>        rgb(187, 68, 172) 
    
    176+11 = 187, 
    4*16= 64 = 68
    160+12 = 172


2. que donneront les codes couleurs suivants ?


    #00FF00             => du pur vert
    rgb(200,200,200)    => du gris foncé
    
voir également http://hex.colorrrs.com/