/**
 * Created by "Lucy Linder" on 17/11/16.
 */

$( function(){
    var WORDS = ['chat', 'informatique', 'versatile'];
    var LIVES = 4;

    var lettersGuessed, currentWord, currentLife;

    var messages = {
        match: '',
        miss: 'Too bad. Try again.',
        twice: 'You already tried this one... Focus, damn it!',
        wrongInput: 'Not a lowercase letter',
        won: 'You won!',
        lost: 'You lose.. ahahah!',
    };

    var output = $('#output');
    var input = $('#letter');
    var restart = $('#restart');

    // setup event handlers
    $(restart).on('click', setup);  // restart --> setup

    $(input).on( 'keydown', function(e){ // new letter
        output.hide(); // hide the output
        var keyCode = event.keyCode ? event.keyCode : event.which; // browser specific
        if (keyCode  == 13) {  // enter key
            var letter = $(this).val();  // get the letter
            if(letter.length != 1 || letter < 'a' || letter > 'z'){
                // not a lowercase letter
                setMessage(messages.wrongInput);
            } else{
                // a lowercase letter, do the work
                guessing(letter);
            }
            // clean the input
            $(this).val('');
        }
    } );

    // launch the game
    setup();

    // -----------------------------------

    // launch a new party
    function setup(){
        var r = ~~(Math.random() * WORDS.length);
        currentWord = WORDS[r];
        lettersGuessed = "";
        currentLife = LIVES;
        drawLetters(currentWord);
        drawScore(currentLife);
        $('.gameOn').fadeIn();
    }

    // draw the letters on a new word
    function drawLetters(word){
        word = word.toUpperCase();
        var ul = $("#letters");
        ul.html(''); // remove previous content

        ul.append('<li class="current-word">Current word:</li>');

        for(var i = 0; i < word.length; i++){
            var letter = word.charAt(i);
            // add a .letter<LETTER> class to every li element
            ul.append($('<li class="letter letter' + letter+ '">' + letter + '</li>'))
        }
    }

    // the real game logic
    function guessing(letter){
        if(lettersGuessed.indexOf(letter) >= 0){
            // letter already tried
            setMessage(messages.twice);
            return;
        }
        // keep track of the letters already guessed
        lettersGuessed += letter;

        // get the letters matching 'letter'
        var lis = $('.letter' + letter.toUpperCase() );
        if(lis.length == 0){
            // letter not in the word
            currentLife--;
            setMessage(messages.miss);
            drawScore(currentLife);
        }else{
            // mark the letters as guessed
            lis.addClass('correct');
            setMessage(messages.match);
        }

        // finally, check for win/lose
        var corrects = $('.letter.correct');
        if(corrects.length == currentWord.length){
            setMessage(msg.won);
            $('.gameOn').fadeOut();
        }else if(currentLife <= 0){
            setMessage(messages.lost);
            $('.gameOn').fadeOut();
        }
    }

    // display a message
    function setMessage(msg){
        output.text(msg).show().fadeIn();
    }

    // update the displayed score
    function drawScore(){
        $('#score').text(currentLife + ' lives remaining');
    }

} );
