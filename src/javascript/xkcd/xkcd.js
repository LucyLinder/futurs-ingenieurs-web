/**
 * Created by "Lucy Linder" on 17/11/16.
 */

$( function(){
    // constants
    var MAX_XKCD = 900;

    // variables
    var $error = $( '#error' );
    var $container = $( '#xkcd-container' );
    var $button = $( '#generate' );

    // initialise page
    $button.on( 'click', randomXKCD );
    randomXKCD();

    // --------------------------

    /**
     * display the XKCD comic using jQuery
     * @param json the XKCD info
     */
    function displayXKCD( json ){
        $error.fadeOut(); // in case

        var title = json.title;
        if( json.link ){
            title = '<a href="' + json.link + '">' + title + '</a>';
        }

        var head = $( '<div class="head"></div>' );
        head.append( '<h1>' + title + '</h1>' );
        head.append( '<span class="num">' + json.num + '</span>' );
        head.append( '<span class="date">' + json.day + '/' + json.month + '/' + json.year + '</span>' );

        var content = $( '<div class="content"></div>' );
        content.append( '<a href="' + json.img + '" target="_blank"><img src="' + json.img + '" title="' + json.alt + '"></a>' );
        content.append( '<div class="alt">' + json.alt + '</div>' );
        content.append( '<div class="transcript">' + json.transcript + '</div>' );

        $container.fadeOut( 300 ).promise().done( function(){
            $( this )
                .html( "" )
                .append( head )
                .append( content )
                .delay( 300 ) // wait a bit for the image to load
                .fadeIn();
        } );

    }

    /**
     * generate random XKCD url
     * @returns {string} the url
     */
    function getRandomLink(){
        var r = ~~(Math.random() * MAX_XKCD + 1);
        return "http://dynamic.xkcd.com/api-0/jsonp/comic/" + r + "?callback=?";
    }

    /**
     * ajax call to retrieve random XKCD comic in json format
     */
    function randomXKCD(){
        var url = getRandomLink();
        $.getJSON( url )
            .done( displayXKCD )
            .fail( function( err ){
                $error.text( err.statusText ).fadeIn();
            } );
    }
} );

