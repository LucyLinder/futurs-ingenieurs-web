/**
 * Created by "Lucy Linder" on 28/11/16.
 */

(function(){
    function animate_string( id ){
        var element = document.getElementById( id );
        var textNode = element.childNodes[0]; // assuming no other children
        var text = textNode.data;

        setInterval( function(){
            text = text[text.length - 1] + text.slice(0,-1);
            text[-1] = '';
            textNode.data = text;
        }, 1000 );
    }

    animate_string( "str" );
})();
