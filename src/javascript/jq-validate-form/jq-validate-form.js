/**
 * Created by "Lucy Linder" on 20/11/16.
 */

$( function(){

    var $name = $( 'input[name=name]' );
    var $age = $( 'input[name=age]' );
    var $checkbox = $( 'input[name=checkbox]' );
    var $output = $( '#output' );

    function validate(){
        var errors = [];

        var name = $name.val();
        if( !name || name.length < 4 ){
            errors.push( 'nom trop court' );
        }

        var age = parseInt( $age.val() );
        if( isNaN( age ) || age < 6 || age > 100 ){
            errors.push( 'age incorrect' );
        }

        var check = $checkbox.is( ':checked' );
        if( !check ){
            errors.push( 'condition doivent être acceptées' );
        }

        if( errors.length ){
            $output.html( errors.join( '<br>' ) );
        }else{
            $output.text( "OK" );
        }
    }

    // -------- listener
    $( 'button' ).click( function(){
        validate();
        return false;
    } );
} );
