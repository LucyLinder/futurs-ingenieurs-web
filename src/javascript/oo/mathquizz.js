/**
 * Created by "Lucy Linder" on 29/11/16.
 */

(function(){
    function MathQuizz( operator, max ){
        this.op = operator || "+";
        this.max = max || 10;
        this.i = 0;
        this.j = 0;
        this.answer = 0;
    }

    MathQuizz.prototype.newQuestion = function(){
        this.i = ~~(Math.random() * this.max) + 1;
        this.j = ~~(Math.random() * this.max) + 1;
        var str = this.i + this.op + this.j;
        console.log( str, "=?" );
        this.answer = eval( str );
    };

    MathQuizz.prototype.getAnswer = function(){
        return this.answer;
    };
})();


(function(){
    function MathQuizz( operator, max ){
        this.op = operator || "+";
        this.max = max || 10;
        this.i = 0;
        this.j = 0;
        this.answer = 0;

        this.newQuestion = function(){
            this.i = ~~(Math.random() * this.max) + 1;
            this.j = ~~(Math.random() * this.max) + 1;
            var str = this.i + this.op + this.j;
            console.log( str, "=?" );
            this.answer = eval( str );
        };

        this.getAnswer = function(){
            return this.answer;
        };
    }
})();


(function(){
    var mathQuizz = {
        op: "+", max: 10,
        i : 0, j: 0, answer: 0,

        newQuestion: function(){
            this.i = ~~(Math.random() * this.max) + 1;
            this.j = ~~(Math.random() * this.max) + 1;
            var str = this.i + this.op + this.j;
            console.log( str, "=?" );
            this.answer = eval( str );
        },

        getAnswer: function(){
            return this.answer;
        }
    }
})();