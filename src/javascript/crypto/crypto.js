/**
 * Created by "Lucy Linder" on 20/11/16.
 */

$(function(){
    $('#rotate').click(rotate);
    var rotation = 0;

    function rotate(){
        rotation += 90;
        $('#mask').css( 'transform', 'rotate(' + rotation + 'deg)' );
    }

    $('#container').click(function(e){
        console.log(e.target);
    });
});