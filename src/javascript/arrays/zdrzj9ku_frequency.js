/**
 * Created by "Lucy Linder" on 19/11/16.
 */

console.clear();
var sentence1 = "Pasta ipsum dolor sit amet maltagliati fagioloni spaghettini sagnarelli gramigna. Gomito lasagnette tortelloni creste di galli creste di galli lagane acini di pepe mezzani pasta zitoni tuffoli tortelloni ricciutelle bavettine calamarata. Tuffoli sacchettoni campanelle tortellini sagne 'ncannulate penne trofie creste di galli trenette gigli mezze penne ravioli capellini garganelli strozzapreti.";

var sentence2 = "aaaa b a ewrwet aa";

function frequency3( sentence, letter ){
    sentence = sentence.split( '' ).sort().join( '' );
    var i = sentence.indexOf( letter );
    var j = sentence.lastIndexOf( letter );
    return i < 0 ? 0 : j + 1 - i;
}

function frequency2( sentence, letter ){
    sentence = sentence.split( '' ).sort().join( '' );
    var i = sentence.indexOf( letter );
    var j = sentence.lastIndexOf( letter );
    return i < 0 ? 0 : j + 1 - i;
}

function frequency( sentence, letter ){
    return sentence.split( letter ).length + 1;
}


test();

// ----------------- unit test


function test(){
    var ok = 0;
    var tests = {
        a: 40,
        b: 1,
        c: 12,
        d: 5
    };
    for( var l in tests ){
        if( tests[l] !== frequency( sentence1, l ) ){
            console.log( "FAILED for letter " + l );
        }else{
            ok++;
        }
    }

    console.log( ok == 4 ? "bravo" : "il faut encore essayer..." );
}
