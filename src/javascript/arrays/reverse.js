/**
 * Created by "Lucy Linder" on 19/11/16.
 */


function reverse( s ){
    return s.split().reverse().join();
}
