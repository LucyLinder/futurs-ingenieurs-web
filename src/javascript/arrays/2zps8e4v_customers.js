/**
 * Created by "Lucy Linder" on 19/11/16.
 */

console.clear();

var customers = [{
    firstname: 'Joe',
    lastname : 'Blogs'
}, {
    firstname: 'John',
    lastname : 'Smith'
}, {
    firstname: 'Dave',
    lastname : 'Jones'
}, {
    firstname: 'Jack',
    lastname : 'White'
}];

// 1. créez une array d'initiales: ["JB", "JS", "DJ", "JW"]
var initials = customers.map( function( c ){
    return c.firstname.charAt( 0 ) + c.lastname.charAt( 0 );
} );
log( "initials:", initials );

// 2. filtrez les customers dont le prénom commence par J
var customersJ = customers.filter( function( c ){
    return c.firstname.startsWith( "J" );
} );
log("customers J:", customersJ);

// 3. triez les customers par nom de famille ascendant
customers.sort(function(a,b){
    if(a.lastname == b.lastname) return 0;
    return a.lastname < b.lastname ? -1 : 1;
});
log("sort lastnames", customers);

// 4. en utilisant ce que vous avez fait plus haut, écrivez une expression capable de :
//    retourner une array triée de noms de tous les customers dont le prénom possède un 'e'
//    résultat: [{name:"Dave Jones"},{name:"Joe Blogs"}]

var projection = customers
    .filter(function(c){ return c.firstname.indexOf("e") >= 0; })
    .map(function(c){ return {name: c.firstname + " " + c.lastname}; })
    .sort(function(a,b){ return a.name == b.name ? 0 : (a.name <  b.name ? -1 : 1); });
log("projection:", projection);
// ----------------------------------

function log( msg, obj ){
    console.log( "---------------" );
    console.log( msg, JSON.stringify(obj) );
}