/**
 * Created by "Lucy Linder" on 19/11/16.
 */

// 1. écrivez une fonction qui détermine si un mot est un palindrome:
//    isPalindrome("bnobonb") = true
//    isPalindrome("a") = true
//    isPalindrome("hello") = false

function isPalindrome(s){
    return s.split('').reverse().join() == s;
}

function isPalindrome2( s ){
    var i = 0, j = s.length - 1;
    while( i < j ){
        if( s[i] != s[j] ) return false;
        i++;
        j--;
    }
    return true;
}


// ------------------------ Unit test

var tests = {
    "abccba": true,
    "abc1cba": true,
    "a": true,
    "abcabc": false,
    "": true,
    "aaab": false
};
console.clear();
var results = Object.keys(tests).map(function(t) {
    return (isPalindrome(t) == tests[t] ? "OK    " : "FAILED") + " : '" + t + "'";
});
console.log(results.join('\n'));

