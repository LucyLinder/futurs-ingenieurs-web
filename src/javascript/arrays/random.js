/**
 * Created by "Lucy Linder" on 28/11/16.
 */

// Returns a random integer between min (included) and max (included)
function getRandomInclusive( min, max ){
    return ~~(Math.random() * (max - min + 1)) + min;
}

// Returns a random integer between min (included) and max (included)
function getRandomExclusive( min, max ){
    return ~~(Math.random() * (max - min)) + min;
}

function randomElt( arr ){
    return arr[getRandomExclusive( 0, arr.length )];
}