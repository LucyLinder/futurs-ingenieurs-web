/**
 * Created by "Lucy Linder" on 19/11/16.
 */

console.clear();

// set up arrays
var numbers = [1, 12, 4, 18, 9, 7, 11, 3, 101, 5, 6];
var strings = ['this', 'is', 'a', 'collection', 'of', 'words'];

// 1. utiliser sort pour trier numbers. Quel est le résultat ?
numbers.sort();
log( "sort: ", numbers );
reset();

// 2. trier numbers par ordre croissant.
numbers.sort(function(a,b){ return a-b;});
log( "sort ascending: ", numbers );
reset();

// 3. trier numbers par ordre décroissant
numbers.sort(function(a,b){ return b-a;});
log( "sort descending:", numbers );
reset();

// 4. trier strings selon la longueur de chaque mot (ascendant)
strings.sort(function(a,b){ return a.length - b.length;});
log( "sort strings length:", strings );
reset();

// 5. créez une nouvelle array avec tous les numéros dans numbers divisibles par 3
var n3 = numbers.filter(function(a){ return a%3 == 0;});
log( "numbers%3:", n3 );

// 6. créez une array avec tous les mots de strings qui contiennent "is"
var sIs = strings.filter(function(a){ return a.indexOf("is") >= 0;});
log( "strings is:", sIs );

// 7. transformez strings en une chaine de charactères avec un appel de fonction
var sentence = strings.join(" ");
log("sentence:", sentence);

// 8. créez la chaine de caractères "4*18*9" en utilisant numbers
var n4189 = numbers.slice(2,5).join("*");
log("4*18*9:", n4189);

// 9. extraire [3, 101, 5] sans modifier numbers. Que changer pour modifier numbers directement ?

var n31015 = numbers.slice(-4, -1);
log("[3, 101, 5]:", n31015);

// 10. utilisez map pour transformer un mot sur deux en majuscules.
//     indice: si vous utilisez map(function(e,i){}), e = élément et i = index !

var sS = strings.map(function(e,i){ return i%2 == 0 ? e.toUpperCase() : e; });
log("strings mixt case:", sS);


// 11. trouvez au moins trois techniques différentes pour remplacer le premier mot dans strings par "dog"
strings.shift(); strings.unshift("dog");
log("dog 1:", strings);
reset();

var dog2 = strings.map(function(e,i){ return i == 0 ? "dog" : e; });
log("dog 2:", dog2);

var dog3 = strings.join(' ').substr(4);
dog3 = ("dog" + dog3).split(" ");
log("dog 3:", dog3);



// ----------------------------------

function log( msg, obj ){
    console.log( "---------------" );
    console.log( msg, obj );
}

function reset(){
    numbers = [1, 12, 4, 18, 9, 7, 11, 3, 101, 5, 6];
    strings = ['this', 'is', 'a', 'collection', 'of', 'words'];
}