/**
 * Created by "Lucy Linder" on 20/11/16.
 */

function generate(){
    var csv = $( 'textarea' ).val();
    if( !(csv && csv.trim().length) ){
        return;
    }
    var $table = $( '<table></table>' );
    var lines = csv.split( "|" );

    $.each( lines, function( i, line ){
        var $tr = $( '<tr></tr>' );
        var cells = line.split( ',' );
        $.each( cells, function( i, cell ){
            $tr.append( '<td>' + cell + '</td>' )
        } );
        $table.append( $tr );
    } );

    $( '#output' ).html( $table );
}

$( function(){
    $( '#generate' ).click( generate );
} );

