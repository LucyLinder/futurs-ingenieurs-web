// source: https://github.com/macloo/jquery_exercises
// NOTE If you really need dragging, use jQuery UI. See:
// http://jqueryui.com/draggable/

$(document).ready(function() {

    // $dragging is only a variable, could be $apple
    var $dragging = null;
    var zindex = 0;
    $(document).on('mousemove', function(e) {
        // NOTE e here stands for event; the position updates as the mouse moves
        if ($dragging) {
            $dragging.offset({
                top: e.pageY - 100, // do not use px here
                left: e.pageX - 100
            });
            // x.offset() is a JS method that works on any object
            // pageY - http://api.jquery.com/event.pagey/
            // pageX - http://api.jquery.com/event.pagex/
        }
    });

    $(document).on('mousedown', '.dragger', function(e) {
        // value of $dragging becomes the div that you clicked
        $dragging = $(e.target);
        $dragging.css("z-index", ++zindex);

    });

    $(document).on('mouseup', function(e) {
        $dragging.removeClass("dragging");
        $dragging = null;
    });

});